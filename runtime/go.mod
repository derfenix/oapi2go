module gitlab.com/derfenix/oapi2go/runtime

go 1.14

require (
	github.com/getkin/kin-openapi v0.35.0
	github.com/stretchr/testify v1.6.1
)
