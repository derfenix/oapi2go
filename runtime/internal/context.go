package internal

type ctxKey int

// LoggerCtx key used to store ErrLogger implementations in context.
const LoggerCtx ctxKey = 1
