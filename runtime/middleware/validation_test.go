package middleware

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3filter"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/derfenix/oapi2go/runtime"
)

type testLogger struct {
	ch chan string
}

func (t *testLogger) Error(err string) {
	t.ch <- err
}

func reqWithLogger(r *http.Request, ch chan string) *http.Request {
	return r.WithContext(runtime.ContextWithErrLogger(r.Context(), &testLogger{ch: ch}))
}

func TestValidationMiddleware(t *testing.T) {
	t.Parallel()

	const pet = `{"id": 1, "name": "foo", "tag": "bar"}`
	const pets = "[" + pet + "]"

	spec, err := openapi3.NewSwaggerLoader().LoadSwaggerFromFile("./testdata/petstore-expanded.yaml")
	require.NoError(t, err)

	defaultOptions := Options{
		Options: openapi3filter.Options{
			ExcludeRequestBody:    false,
			ExcludeResponseBody:   false,
			IncludeResponseStatus: true,
			MultiError:            true,
			AuthenticationFunc:    nil,
		},
		ValidateResponse: true,
	}

	t.Run("request validation", func(t *testing.T) {
		reqBuilder := func(method, path string, data io.Reader) *http.Request {
			path = fmt.Sprintf("http://petstore.swagger.io/api%s", path)

			req := httptest.NewRequest(method, path, data)

			req.Header.Set("Content-Type", "application/json")

			return req
		}

		noopHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			w.Header().Set("Content-Type", "application/json")

			_, err := w.Write([]byte(pets))
			require.NoError(t, err)
		})

		tests := []struct {
			name    string
			options *Options
			method  string
			path    string
			data    string

			wantStatus       int
			wantResponseData string
		}{
			{
				name:    "get request",
				options: &defaultOptions,
				method:  http.MethodGet,
				path:    "/pets",
				data:    "",

				wantStatus:       200,
				wantResponseData: pets,
			},

			{
				name:    "bad param type",
				options: &defaultOptions,
				method:  http.MethodGet,
				path:    "/pets?limit=bad",
				data:    "",

				wantStatus:       400,
				wantResponseData: "Parameter 'limit' in query has an error: value bad: an invalid integer: strconv.ParseFloat: parsing \"bad\": invalid syntax",
			},
		}

		for _, test := range tests {
			test := test

			t.Run(test.name, func(t *testing.T) {
				t.Parallel()

				middleware := ValidationMiddleware(spec, test.options)
				writer := httptest.NewRecorder()

				middleware(noopHandler).ServeHTTP(writer, reqBuilder(test.method, test.path, bytes.NewBufferString(test.data)))

				assert.Equal(t, test.wantStatus, writer.Code)
				assert.Equal(t, test.wantResponseData, string(writer.Body.Bytes()))
			})
		}
	})

	t.Run("response validation", func(t *testing.T) {
		t.Run("logging bad response schema", func(t *testing.T) {
			t.Parallel()

			middleware := ValidationMiddleware(spec, &defaultOptions)
			writer := httptest.NewRecorder()

			req := httptest.NewRequest(http.MethodGet, "http://petstore.swagger.io/api/pets", nil)

			req.Header.Set("Content-Type", "application/json")

			logCh := make(chan string, 1)
			req = reqWithLogger(req, logCh)

			middleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Header().Set("Content-Type", "application/json")
				_, err := w.Write([]byte(`[{"id": "foo"}]`))
				require.NoError(t, err)
			})).ServeHTTP(writer, req)

			assert.Equal(t, http.StatusOK, writer.Code)
			assert.Equal(t, "[{\"id\": \"foo\"}]", string(writer.Body.Bytes()))

			select {
			case m := <-logCh:
				assert.Contains(t, m, "response error: response body doesn't match the schema:")
			case <-time.After(time.Second):
				t.Error("no log item received")
			}
		})

		t.Run("logging bad status code", func(t *testing.T) {
			t.Parallel()

			middleware := ValidationMiddleware(spec, &defaultOptions)
			writer := httptest.NewRecorder()

			req := httptest.NewRequest(http.MethodGet, "http://petstore.swagger.io/api/pets", nil)

			req.Header.Set("Content-Type", "application/json")

			logCh := make(chan string, 1)
			req = reqWithLogger(req, logCh)

			middleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
				w.Header().Set("Content-Type", "application/json")
				_, err := w.Write([]byte(pets))
				require.NoError(t, err)
			})).ServeHTTP(writer, req)

			assert.Equal(t, http.StatusNoContent, writer.Code)

			select {
			case m := <-logCh:
				assert.Equal(t, "response error: status is not supported", m)
			case <-time.After(time.Second):
				t.Error("no log item received")
			}
		})
	})
}
