package middleware

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3filter"

	"gitlab.com/derfenix/oapi2go/runtime"
)

var reqBodiesPool = sync.Pool{
	New: func() interface{} {
		return &bytes.Buffer{}
	},
}

type Options struct {
	openapi3filter.Options

	ValidateResponse bool
}

func newRw(w http.ResponseWriter) *rw {
	return &rw{ResponseWriter: w, body: reqBodiesPool.Get().(*bytes.Buffer)}
}

type rw struct {
	http.ResponseWriter

	code    int
	headers http.Header
	body    *bytes.Buffer
}

func (w *rw) Write(b []byte) (int, error) {
	w.body.Write(b)

	return w.ResponseWriter.Write(b)
}

func (w *rw) WriteHeader(code int) {
	w.code = code

	w.ResponseWriter.WriteHeader(code)
}

func (w *rw) Header() http.Header {
	return w.ResponseWriter.Header()
}

// ValidationMiddleware middleware that validates request and responses bodies, statuses and authentication credentials.
func ValidationMiddleware(spec *openapi3.Swagger, opts *Options) func(http.Handler) http.Handler {
	router := openapi3filter.NewRouter().WithSwagger(spec)

	logWerr := func(log runtime.ErrLogger, err error) {
		if err != nil {
			log.Error(fmt.Sprintf("failed to write error response: %s", err.Error()))
		}
	}

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log := runtime.ErrLoggerFromContext(r.Context())
			if log == nil {
				log = runtime.DefaultLogger()
			}

			if opts.ValidateResponse {
				rwx := newRw(w)
				w = rwx

				defer func() {
					rwx.body.Reset()
					reqBodiesPool.Put(rwx.body)
				}()
			}

			// Find route
			route, pathParams, err := router.FindRoute(r.Method, r.URL)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)

				var routeErr *openapi3filter.RouteError
				if errors.As(err, &routeErr) {
					_, werr := w.Write([]byte(routeErr.Reason))
					logWerr(log, werr)
				}

				return
			}

			requestValidationInput := &openapi3filter.RequestValidationInput{
				Request:    r,
				PathParams: pathParams,
				Route:      route,
				Options:    &opts.Options,
			}

			if err := openapi3filter.ValidateRequest(r.Context(), requestValidationInput); err != nil {
				var (
					errTxt  string
					authErr *openapi3filter.SecurityRequirementsError
					reqErr  *openapi3filter.RequestError
				)

				switch {
				case errors.As(err, &authErr):
					w.WriteHeader(http.StatusUnauthorized)
					errTxt = fmt.Sprintf("%s: %s", authErr.Error(), openapi3.MultiError(authErr.Errors).Error())

				case errors.As(err, &reqErr):
					w.WriteHeader(reqErr.HTTPStatus())
					errTxt = reqErr.Error()

				default:
					w.WriteHeader(http.StatusInternalServerError)
					errTxt = err.Error()
				}

				log.Error(errTxt)

				_, werr := w.Write([]byte(errTxt))
				logWerr(log, werr)

				return
			}

			next.ServeHTTP(w, r)

			if opts.ValidateResponse {
				rwx := w.(*rw)

				responseValidationInput := &openapi3filter.ResponseValidationInput{
					RequestValidationInput: requestValidationInput,
					Status:                 rwx.code,
					Header:                 rwx.Header(),
					Options:                &opts.Options,
					Body:                   ioutil.NopCloser(rwx.body),
				}

				// Validate response.
				if err := openapi3filter.ValidateResponse(r.Context(), responseValidationInput); err != nil {
					var respErr *openapi3filter.ResponseError

					switch {
					case errors.As(err, &respErr):
						log.Error(fmt.Sprintf("response error: %s", respErr.Error()))

					default:
						log.Error(err.Error())
					}
				}
			}
		})
	}
}
