package runtime

import (
	"context"
	"log"

	"gitlab.com/derfenix/oapi2go/runtime/internal"
)

// ErrLogger simple error-only logging interface.
type ErrLogger interface {
	Error(err string)
}

type stdLogger struct{}

func (n *stdLogger) Error(err string) {
	log.Println("ERR: ", err)
}

// DefaultLogger returns ErrLogger implementation with std log.Println as actual logger.
func DefaultLogger() *stdLogger {
	return &stdLogger{}
}

func ContextWithErrLogger(ctx context.Context, logger ErrLogger) context.Context {
	return context.WithValue(ctx, internal.LoggerCtx, logger)
}

func ErrLoggerFromContext(ctx context.Context) ErrLogger {
	if logger, ok := ctx.Value(internal.LoggerCtx).(ErrLogger); ok {
		return logger
	}

	return nil
}

func MustErrLoggerFromContext(ctx context.Context) ErrLogger {
	if logger := ErrLoggerFromContext(ctx); logger != nil {
		return logger
	}

	panic("no ErrLogger in context")
}
