package types

import (
	"bytes"
	"errors"
	"fmt"
	"regexp"
)

var (
	// ErrInvalidEmailFormat provided email not passed validation.
	ErrInvalidEmailFormat = errors.New("invalid email format")
	// ErrInvalidEmailLength length of email is less or greater than required.
	ErrInvalidEmailLength = errors.New("invalid email length")
)

// W3C expired email regexp.
//
//goland:noinspection RegExpRedundantEscape
var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// Email simple email type, validates only @ symbol in string.
type Email string

func (e *Email) UnmarshalJSON(data []byte) error {
	if data[0] == '"' {
		if data[len(data)-1] != '"' {
			return fmt.Errorf("%w: %s started with double quotes, but ends without", ErrInvalidStringProvided, string(data))
		}

		data = data[1 : len(data)-1]
	}

	if !bytes.ContainsRune(data, '@') {
		return ErrInvalidEmailFormat
	}

	*e = Email(data)

	return nil
}

// RegexpEmail strict email type with validation by regexp, provided by W3C.
//
// Link: https://www.w3.org/TR/2016/REC-html51-20161101/sec-forms.html#email-state-typeemail
type RegexpEmail string

func (e *RegexpEmail) UnmarshalJSON(data []byte) error {
	if data[0] == '"' {
		if data[len(data)-1] != '"' {
			return fmt.Errorf("%w: %s started with double quotes, but ends without", ErrInvalidStringProvided, string(data))
		}

		data = data[1 : len(data)-1]
	}

	if len(data) < 3 && len(data) > 254 {
		return fmt.Errorf("%w: %d", ErrInvalidEmailLength, len(data))
	}

	if !emailRegex.Match(data) {
		return ErrInvalidEmailFormat
	}

	*e = RegexpEmail(data)

	return nil
}
