package types

import (
	"testing"
)

func TestEmail_UnmarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		e       Email
		arg     []byte
		wantErr bool
	}{
		{
			name:    "normal email 1",
			e:       Email("some@gmail.com"),
			arg:     []byte("some@gmail.com"),
			wantErr: false,
		},

		{
			name:    "normal email 2",
			e:       Email("some@gmail"),
			arg:     []byte("some@gmail"),
			wantErr: false,
		},

		{
			name:    "normal email cyrillic",
			e:       Email("моя@почта.ru"),
			arg:     []byte("моя@почта.ru"),
			wantErr: false,
		},

		{
			name:    "bad email",
			e:       Email(""),
			arg:     []byte("somegmail"),
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.e.UnmarshalJSON(tt.arg); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRegexpEmail_UnmarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		e       RegexpEmail
		arg     []byte
		wantErr bool
	}{
		{
			name:    "normal email 1",
			e:       RegexpEmail("some@gmail.com"),
			arg:     []byte("some@gmail.com"),
			wantErr: false,
		},

		{
			name:    "normal email 2",
			e:       RegexpEmail("some@gmail"),
			arg:     []byte("some@gmail"),
			wantErr: false,
		},

		{
			name:    "invalid email cyrillic",
			e:       RegexpEmail("моя@почта.ru"),
			arg:     []byte("моя@почта.ru"),
			wantErr: true,
		},

		{
			name:    "bad email",
			e:       RegexpEmail(""),
			arg:     []byte("somegmail"),
			wantErr: true,
		},

		{
			name:    "too short",
			e:       RegexpEmail(""),
			arg:     []byte("sd"),
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.e.UnmarshalJSON(tt.arg); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
