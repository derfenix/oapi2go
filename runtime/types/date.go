package types

import (
	"bytes"
	"fmt"
	"time"
)

const dateFormat = "2006-01-02"

// Date represents date-only time format.
type Date struct {
	time.Time
}

func (d Date) MarshalJSON() ([]byte, error) {
	b := bytes.Buffer{}
	b.Grow(len(dateFormat) + 2)

	b.WriteRune('"')
	b.WriteString(d.Time.Format(dateFormat))
	b.WriteRune('"')

	return b.Bytes(), nil
}

func (d *Date) UnmarshalJSON(data []byte) error {
	if data[0] == '"' {
		if data[len(data)-1] != '"' {
			return fmt.Errorf("%w: %s started with double quotes, but ends without", ErrInvalidStringProvided, string(data))
		}

		data = data[1 : len(data)-1]
	}

	parsed, err := time.Parse(dateFormat, string(data))
	if err != nil {
		return fmt.Errorf("parse date string: %w", err)
	}

	d.Time = parsed

	return nil
}
