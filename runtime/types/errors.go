package types

import (
	"errors"
)

// ErrInvalidStringProvided generic error about malformed strings provided for un-marshaling.
var ErrInvalidStringProvided = errors.New("invalid string provided")
