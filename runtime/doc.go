/*
Package runtime provides functions, types and interfaces required for using generated server and/or client.
*/
package runtime
