package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var baseTemplate = `package %s

import (
	"fmt"
	"text/template"
)

var templates = map[string]string{
%s
}

func GetTemplate(t string) (*template.Template, error) {
	data, ok := templates[t]
	if !ok {
		return nil, fmt.Errorf("template %%s not found", t)
	}

	tmpl, err := template.New(t).Funcs(funcMap).Parse(data)

	return tmpl, err
}
`

func main() {
	buf, err := merge()
	if err != nil {
		panic(err)
	}

	cwd, err := os.Getwd()
	if err != nil {
		panic(fmt.Errorf("failed to get working directory: %w", err))
	}

	_, pkgName := path.Split(cwd)

	f, err := os.OpenFile(path.Join(".", "templates.gen.go"), os.O_TRUNC|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		panic(fmt.Errorf("open dest file: %w", err))
	}

	defer func() {
		if err := f.Close(); err != nil {
			panic(fmt.Errorf("close file error: %w", err))
		}
	}()

	if _, err := fmt.Fprintf(f, baseTemplate, pkgName, buf.String()); err != nil {
		panic(fmt.Errorf("write file: %w", err))
	}
}

func merge() (res bytes.Buffer, err error) {
	files, err := allTemplatesInDir(".")
	if err != nil {
		return res, fmt.Errorf("get files list: %w", err)
	}

	var buf []byte
	templates := make([][]byte, len(files))

	for idx := range files {
		file, err := os.Open(path.Join(".", files[idx].Name()))
		if err != nil {
			return res, fmt.Errorf("open file %s: %w", files[idx].Name(), err)
		}

		buf, err = ioutil.ReadAll(file)
		if err != nil {
			return res, fmt.Errorf("read file %s: %w", files[idx].Name(), err)
		}

		templates[idx] = writeTemplate(files[idx].Name(), buf).Bytes()
	}

	if _, err := res.Write(bytes.Join(templates, []byte("\n"))); err != nil {
		return res, fmt.Errorf("write template: %w", err)
	}

	return res, nil
}

func writeTemplate(name string, data []byte) *bytes.Buffer {
	name = fileNameWithoutExtension(name)

	sb := bytes.Buffer{}
	sb.WriteRune('\t')
	sb.WriteRune('"')
	sb.WriteString(name)
	sb.WriteRune('"')
	sb.WriteString(": `")
	sb.WriteString(string(data))
	sb.WriteRune('`')
	sb.WriteRune(',')

	return &sb
}

func allTemplatesInDir(d string) (files []os.FileInfo, err error) {
	files, err = ioutil.ReadDir(d)
	if err != nil {
		return nil, fmt.Errorf("read dir: %w", err)
	}

	filteredFiles := make([]os.FileInfo, 0, len(files))
	for idx := range files {
		switch {
		case files[idx].IsDir():
			continue

		case !strings.HasSuffix(files[idx].Name(), ".gotmpl"):
			continue

		default:
			filteredFiles = append(filteredFiles, files[idx])
		}
	}

	return filteredFiles, nil
}

func fileNameWithoutExtension(fileName string) string {
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}
