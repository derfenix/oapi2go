package util

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/pkg/errors"
)

var ErrUnsupportedExtention = errors.New("not a supported extension, use .yaml, .yml or .json")

func LoadSwagger(filePath string) (*openapi3.Swagger, error) {
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var swagger *openapi3.Swagger

	ext := strings.ToLower(filepath.Ext(filePath))
	switch ext {
	// The YAML handler can parse both YAML and JSON
	case ".yaml", ".yml", ".json":
		swagger, err = openapi3.NewSwaggerLoader().LoadSwaggerFromData(data)
	default:
		return nil, fmt.Errorf("%s: %w ", ext, ErrUnsupportedExtention)
	}

	if err != nil {
		return nil, err
	}

	return swagger, nil
}
