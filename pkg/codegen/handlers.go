package codegen

type HandlersInfo struct {
	PackageName string
	Operations  []OperationDefinition
	Tags        []string

	TagsMiddleware bool
}
