package codegen

import (
	"fmt"
	"os"

	"gitlab.com/derfenix/oapi2go/pkg/codegen/templates"
)

func GenerateServer(serverInfo *ServerInfo, targetPath string) error {
	f, err := os.OpenFile(targetPath, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("open file: %w", err)
	}

	defer func() {
		if err := f.Close(); err != nil {
			panic(err) // TODO log error instead of panic
		}
	}()

	tmpl, err := templates.GetTemplate("server")
	if err != nil {
		return fmt.Errorf("get template: %w", err)
	}

	if err := tmpl.Execute(f, serverInfo); err != nil {
		return fmt.Errorf("execute template: %w", err)
	}

	return nil
}
