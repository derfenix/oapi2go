package codegen

import (
	"strings"
)

type ServerInfo struct {
	PackageName            string
	ShutdownTimeoutSeconds int

	ServerReadTimeoutSeconds       int
	ServerWriteTimeoutSeconds      int
	ServerIdleTimeoutSeconds       int
	ServerReadHeaderTimeoutSeconds int
	ServerMaxHeaderBytes           int
}

func (s *ServerInfo) LocalImports() string {
	var imports []string

	//goland:noinspection GoNilness
	if len(imports) == 0 {
		return ""
	}

	return "\n\n    " + strings.Join(imports, "\n    ")
}

func (s *ServerInfo) PkgImports() string {
	var imports []string

	//goland:noinspection GoNilness
	if len(imports) == 0 {
		return ""
	}

	return "\n\n    " + strings.Join(imports, "\n    ")
}

func (s *ServerInfo) StdImports() string {
	var imports []string

	//goland:noinspection GoNilness
	if len(imports) == 0 {
		return ""
	}

	return "\n    " + strings.Join(imports, "\n    ")
}
