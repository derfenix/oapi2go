package codegen

import (
	"io/ioutil"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func filesEqual(t *testing.T, f1, f2 string) {
	t.Helper()

	file1, err := ioutil.ReadFile(f1)
	require.NoError(t, err)

	file2, err := ioutil.ReadFile(f2)
	require.NoError(t, err)

	assert.Equal(t, string(file1), string(file2))
}

func Test_serverGen(t *testing.T) {
	t.Parallel()

	t.Run("test 1", func(t *testing.T) {
		t.Parallel()

		p := path.Join(os.TempDir(), "server1.go")

		serverInfo := ServerInfo{
			PackageName:                    "codegen",
			ShutdownTimeoutSeconds:         1,
			ServerReadTimeoutSeconds:       2,
			ServerWriteTimeoutSeconds:      3,
			ServerIdleTimeoutSeconds:       4,
			ServerReadHeaderTimeoutSeconds: 5,
			ServerMaxHeaderBytes:           6,
		}

		require.NoError(t, GenerateServer(&serverInfo, p))

		filesEqual(t, "./testdata/server.gen.1.godata", p)

		require.NoError(t, os.Remove(p))
	})
}
