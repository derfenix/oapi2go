package templates

import (
	"fmt"
	"text/template"
)

var templates = map[string]string{
	"chi_handlers": `{{- /*gotype:gitlab.com/derfenix/oapi2go/pkg/codegen.HandlersInfo*/ -}}package {{ .PackageName }}

import (
	"net/http"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/go-chi/chi"

	"gitlab.com/derfenix/oapi2go/runtime/middleware"
)

// ServerHandlers interface to be implemented as api handler.
type ServerHandlers interface {
	{{/* @formatter:off */ -}}
	{{ range .Operations }}
	// Op{{ .OperationId }} handles {{ .OperationId }} operation.
	{{-  if .Summary | ne "" }}{{ print "\n    //\n" }}{{ .SummaryAsComment}}{{end}}
	Op{{ .OperationId | Title }}(w http.ResponseWriter, r *http.Request)
	{{ end }}{{/*@formatter:on*/}}
}

// region Operation Middlewares
{{ range .Operations }}
// {{ .OperationId }}Ctx middleware that adds context with parameters and options for {{ .OperationId }} operation.
func {{ .OperationId }}Ctx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

{{ end -}}

// endregion
{{if .TagsMiddleware }}
// region Tags
{{ $tagsCount := len .Tags }}{{- if gt $tagsCount 0 }}{{ range .Tags }}
// Tag{{ . | Title }} middleware for tag {{ . }}.
func Tag{{ . | Title }}(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
{{ end }}{{ end }}
// endregion
{{- end }}

// region Mux handling

// Mux create new chi router for all operations, bind provided ServerHandlers implementation's methods.
// If nil mux provided - new one will be created.
func Mux(h ServerHandlers, mux chi.Router) http.Handler {
	if mux == nil {
		mux = chi.NewMux()
	}

	{{ range .Operations -}}
	mux.With(
		{{- $tagsCount := len .Spec.Tags }}{{ if gt $tagsCount 0 }}
		{{ range .Spec.Tags }}Tag{{ . | Title }}, {{ end }}{{ else }}
		{{- end }}
		{{ .OperationId }}Ctx,
	).MethodFunc("{{ .Method }}", "{{ .Path }}", h.Op{{ .OperationId  }})
	{{ end }}
	return mux
}

func MuxWithValidation(h ServerHandlers, mux chi.Router, swagger *openapi3.Swagger, opts *middleware.Options) http.Handler {
	if mux == nil {
		mux = chi.NewMux()
	}

	mux.Use(middleware.ValidationMiddleware(swagger, opts))

	return Mux(h, mux)
}

// endregion
`,
	"models": `{{- /*gotype:gitlab.com/derfenix/oapi2go/pkg/codegen.HandlersInfo*/ -}}package {{ .PackageName }}


`,
	"server": `{{- /*gotype:gitlab.com/derfenix/oapi2go/pkg/codegen.ServerInfo*/ -}}package {{ .PackageName }}

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"{{ .StdImports }}{{ .PkgImports }}

	"github.com/pkg/errors"{{ .LocalImports }}

	"gitlab.com/derfenix/oapi2go/runtime"
)

// Server http server
type Server struct {
	address string
	server  *http.Server
}

func NewServer(addr string) *Server {
	return &Server{
		address: addr,
		server: &http.Server{
			Addr:      addr,
			Handler:   nil,
			TLSConfig: nil,

			ReadTimeout:       {{ .ServerReadTimeoutSeconds | or 30}},
			ReadHeaderTimeout: {{ .ServerReadHeaderTimeoutSeconds | or 30}},

			WriteTimeout:   {{ .ServerWriteTimeoutSeconds | or 30 }},
			IdleTimeout:    {{ .ServerIdleTimeoutSeconds }},
			MaxHeaderBytes: {{ .ServerMaxHeaderBytes }},

			TLSNextProto: nil,
			ConnState:    nil,
			ErrorLog:     nil,
			BaseContext:  nil,
			ConnContext:  nil,
		},
	}
}

func (s *Server) Start(ctx context.Context, wg *sync.WaitGroup, logger runtime.ErrLogger) {
	defer wg.Done()

	if logger == nil {
		logger = runtime.DefaultLogger()
	}

	serverShutdownInterval := {{ .ShutdownTimeoutSeconds | or 5 }}

	stop := make(chan error, 1)
	go func() {
		if err := s.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			stop <- err
		}

		stop <- nil
	}()

	select {
	case <-ctx.Done():
		sCtx, cancel := context.WithTimeout(context.Background(), time.Duration(serverShutdownInterval)*time.Second)
		defer cancel()

		if err := s.server.Shutdown(sCtx); err != nil {
			logger.Error(fmt.Errorf("server shutdown: %w", err).Error())
		}

		return

	case err := <-stop:
		if err != nil {
			logger.Error(fmt.Errorf("server stopped with error: %w", err).Error())
		}
	}
}

func (s *Server) SetHandler(h http.Handler) {
	s.server.Handler = h
}
`,
}

func GetTemplate(t string) (*template.Template, error) {
	data, ok := templates[t]
	if !ok {
		return nil, fmt.Errorf("template %s not found", t)
	}

	tmpl, err := template.New(t).Funcs(funcMap).Parse(data)

	return tmpl, err
}
