package codegen

import (
	"fmt"

	"github.com/getkin/kin-openapi/openapi3"

	"gitlab.com/derfenix/oapi2go/pkg/codegen/internal"
)

const strictEmailExtension = "x-strict-email"

type uses uint32

func (u uses) use(other uses) bool {
	return !(u&other == 0)
}

const (
	useRuntimeType uses = 1 << 0
	useTime        uses = 1 << 1
)

type Models struct {
	PackageName string

	Models []Model
}

func (m *Models) Imports() internal.ImportsSet {
	imps := internal.NewImportsSet()
	use := map[uses]struct{}{}

	for idx := range m.Models {
		if m.Models[idx].UseRuntimeType() {
			use[useRuntimeType] = struct{}{}
		}

		if m.Models[idx].UseTime() {
			use[useTime] = struct{}{}
		}
	}

	for u := range use {
		if u == useRuntimeType {
			imps["locals"] = append(imps["locals"], `runtimeTypes "gitlab.com/derfenix/oapi2go/runtime/types"`)
		}

		if u == useTime {
			imps["std"] = append(imps["std"], `"time"`)
		}
	}

	imps.Sort()

	return imps
}

type Model struct {
	Fields []Field

	Name    string
	Type    string
	Summary string

	uses uses

	StrictEmail bool
}

func NewModelFromOpenAPI(schema openapi3.Schema) Model {
	model := Model{
		Fields: make([]Field, 0, len(schema.Properties)),
	}

	var modelUse uses

	for s, ref := range schema.Properties {
		field, use := newField(s, ref)
		modelUse |= use
		model.Fields = append(model.Fields, field)
	}

	if se, ok := schema.Extensions[strictEmailExtension]; ok && se.(string) == "true" {
		model.StrictEmail = true
	}

	model.uses = modelUse

	return model
}

func (m *Model) UseRuntimeType() bool {
	return m.uses.use(useRuntimeType)
}

func (m *Model) UseTime() bool {
	return m.uses.use(useTime)
}

type Field struct {
	Name    string
	Summary string
	Type    string
	Tags    string

	Deprecated bool
}

func newField(name string, s *openapi3.SchemaRef) (Field, uses) {
	fieldType, use := guessType(s)

	field := Field{
		Name:       name,
		Summary:    s.Value.Title,
		Type:       fieldType,
		Tags:       "",
		Deprecated: s.Value.Deprecated,
	}

	return field, use
}

func guessType(s *openapi3.SchemaRef) (string, uses) {
	const (
		stringType = "string"
		objectType = "object"
	)

	var (
		fieldType string
		use       uses
	)

	if s.Ref != "" {
		return s.Ref, use
	}

	switch s.Value.Type {
	case stringType:
		switch s.Value.Format {
		case "date":
			fieldType = "runtimeTypes.Date"
			use |= useRuntimeType

		case "date-time":
			fieldType = "time.Time"
			use |= useTime

		case "byte":
			fieldType = "[]byte"

		case "double":
			fieldType = "float64"

		case "email":
			if s, ok := s.Value.Extensions[strictEmailExtension]; ok && s.(string) == "true" {
				fieldType = "runtimeTypes.RegexpEmail"
			} else {
				fieldType = "runtimeTypes.Email"
			}

			use |= useRuntimeType

		default:
			fieldType = stringType
		}

	case "integer":
		if s.Value.Format != "" {
			fieldType = s.Value.Format
		} else {
			fieldType = "int"
		}

	case "number":
		fieldType = "float32"

	case "array":
		var arrFt string
		arrFt, use = guessType(s.Value.Items)

		fieldType = fmt.Sprintf("[]%s", arrFt)

	case "boolean":
		fieldType = "bool"

	case objectType:
		fieldType, use = buildStruct(s.Value)
	}

	return fieldType, use
}

func buildStruct(s *openapi3.Schema) (string, uses) {
	// TODO
	return "", 0
}
