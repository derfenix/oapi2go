package internal

import (
	"sort"
	"strings"
)

type ImportsSet map[string]Imports

func NewImportsSet() ImportsSet {
	return ImportsSet{
		"std":      {},
		"external": {},
		"locals":   {},
	}
}

func (i ImportsSet) Sort() {
	sort.Sort(i["std"])
	sort.Sort(i["external"])
	sort.Sort(i["local"])
}

func (i ImportsSet) Std() Imports {
	return i["std"]
}

func (i ImportsSet) External() Imports {
	return i["external"]
}

func (i ImportsSet) Local() Imports {
	return i["local"]
}

type Imports []string

func (i Imports) Len() int {
	return len(i)
}

func (i Imports) Less(x, y int) bool {
	ixs := strings.SplitN(i[x], " ", 2)
	iys := strings.SplitN(i[y], " ", 2)

	ix := ixs[0]
	if len(ixs) == 2 {
		ix = ixs[1]
	}

	iy := iys[0]
	if len(ixs) == 2 {
		iy = iys[1]
	}

	return ix < iy
}

func (i Imports) Swap(x, y int) {
	i[x], i[y] = i[y], i[x]
}

func (i Imports) String() string {
	return strings.Join(i, "\n\t")
}
