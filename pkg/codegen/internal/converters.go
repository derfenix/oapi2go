package internal

import (
	"fmt"
	"strings"
	"unicode"
)

// StringToGoComment renders a possible multi-line string as a valid Go-Comment.
// Each line is prefixed as a comment.
func StringToGoComment(in string) string { // FixMe: Drop or replace?
	// Normalize newlines from Windows/Mac to Linux
	in = strings.ReplaceAll(in, "\r\n", "\n")
	in = strings.ReplaceAll(in, "\r", "\n")

	// Add comment to each line
	var lines []string
	for _, line := range strings.Split(in, "\n") {
		lines = append(lines, fmt.Sprintf("// %s", line))
	}

	in = strings.Join(lines, "\n")

	// in case we have a multiline string which ends with \n, we would generate
	// empty-line-comments, like `// `. Therefore remove this line comment.
	in = strings.TrimSuffix(in, "\n// ")

	return in
}

// ToCamelCase converts query-arg style strings to CamelCase.
//
// We will use `., -, +, :, ;, _, ~, ' ', (, ), {, }, [, ]` as valid delimiters for words.
func ToCamelCase(str string) string { // FixMe: Can be improved?
	const separators = "-#@!$&=.+:;_~ (){}[]"

	var n string

	s := strings.Trim(str, " ")

	capNext := true

	for _, v := range s {
		switch {
		case unicode.IsUpper(v), unicode.IsDigit(v):
			n += string(v)

		case unicode.IsLower(v):
			if capNext {
				n += strings.ToUpper(string(v))
			} else {
				n += string(v)
			}
		}

		if strings.ContainsRune(separators, v) {
			capNext = true
		} else {
			capNext = false
		}
	}

	return n
}

// UppercaseFirstCharacter returns copy of str with first char uppercase. With respect to Unicode.
func UppercaseFirstCharacter(str string) string {
	if str == "" {
		return ""
	}

	runes := []rune(str)
	runes[0] = unicode.ToUpper(runes[0])

	return string(runes)
}

// LowercaseFirstCharacter returns copy of str with first char lowercase. With respect to Unicode.
func LowercaseFirstCharacter(str string) string {
	if str == "" {
		return ""
	}

	runes := []rune(str)
	runes[0] = unicode.ToLower(runes[0])

	return string(runes)
}
