package internal

import (
	"github.com/getkin/kin-openapi/openapi3"
)

// StringInArray returns true if provided string is in provided slice.
func StringInArray(str string, sl []string) bool {
	for _, elt := range sl {
		if elt == str {
			return true
		}
	}

	return false
}

// SchemaHasAdditionalProperties returns true if schema can or do contain additional properties.
func SchemaHasAdditionalProperties(schema *openapi3.Schema) bool {
	if schema.AdditionalPropertiesAllowed != nil {
		return *schema.AdditionalPropertiesAllowed
	}

	if schema.AdditionalProperties != nil {
		return true
	}

	return false
}
