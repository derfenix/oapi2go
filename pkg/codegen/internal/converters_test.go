package internal

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func ExampleToCamelCase() {
	s := "word.word-word+word:word;word_word~word word(word)word{word}[word]"

	converted := ToCamelCase(s)

	fmt.Println(converted)
	// Output: WordWordWordWordWordWordWordWordWordWordWordWordWord
}

func TestStringToGoComment(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		input    string
		expected string
		message  string
	}{
		{
			input:    "",
			expected: "// ",
			message:  "blank string should be preserved with comment",
		},
		{
			input:    " ",
			expected: "//  ",
			message:  "whitespace should be preserved with comment",
		},
		{
			input:    "Single Line",
			expected: "// Single Line",
			message:  "single line comment",
		},
		{
			input:    "    Single Line",
			expected: "//     Single Line",
			message:  "single line comment preserving whitespace",
		},
		{
			input: `Multi
Line
  With
    Spaces
	And
		Tabs
`,
			expected: `// Multi
// Line
//   With
//     Spaces
// 	And
// 		Tabs`,
			message: "multi line preserving whitespaces using tabs or spaces",
		},
	}

	for _, tt := range testCases {
		tt := tt

		t.Run(tt.message, func(t *testing.T) {
			t.Parallel()
			result := StringToGoComment(tt.input)
			assert.EqualValues(t, tt.expected, result, tt.message)
		})
	}
}
