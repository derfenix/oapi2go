package internal

import (
	"sort"

	"github.com/getkin/kin-openapi/openapi3"
)

// SortedSchemaKeys returns SchemaRef map keys in sorted order.
func SortedSchemaKeys(m map[string]*openapi3.SchemaRef) []string {
	keys := make([]string, 0, len(m))

	for key := range m {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	return keys
}

// SortedPathsKeys returns Paths map keys in sorted order.
func SortedPathsKeys(m openapi3.Paths) []string {
	keys := make([]string, 0, len(m))

	for key := range m {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	return keys
}

// SortedOperationsKeys returns Operation map keys in sorted order.
func SortedOperationsKeys(m map[string]*openapi3.Operation) []string {
	keys := make([]string, 0, len(m))

	for key := range m {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	return keys
}

// SortedResponsesKeys returns Responses map keys in sorted order.
func SortedResponsesKeys(m openapi3.Responses) []string {
	keys := make([]string, 0, len(m))

	for key := range m {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	return keys
}

// SortedContentKeys returns Content map keys in sorted order.
func SortedContentKeys(dict openapi3.Content) []string {
	keys := make([]string, 0, len(dict))

	for key := range dict {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	return keys
}

// SortedStringKeys returns strings map keys in sorted order.
func SortedStringKeys(dict map[string]string) []string {
	keys := make([]string, 0, len(dict))

	for key := range dict {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	return keys
}

// SortedParameterKeys returns ParameterRef map keys in sorted order.
func SortedParameterKeys(dict map[string]*openapi3.ParameterRef) []string {
	keys := make([]string, 0, len(dict))

	for key := range dict {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	return keys
}

// SortedRequestBodyKeys returns RequestBodyRef map keys in sorted order.
func SortedRequestBodyKeys(dict map[string]*openapi3.RequestBodyRef) []string {
	keys := make([]string, len(dict))

	for key := range dict {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	return keys
}
