help:
	@echo "This is a helper makefile for oapi2go"
	@echo "Targets:"
	@echo "    generate:    regenerate all generated files"
	@echo "    test:        run all tests"

generate:
	go generate -x ./...

test:
	go test -cover -covermode=atomic -coverprofile=cover.out ./...
	@echo -n "coverage: "
	@go tool cover -func=cover.out | grep total: | awk '{ print $$3 }'
	@rm cover.out
